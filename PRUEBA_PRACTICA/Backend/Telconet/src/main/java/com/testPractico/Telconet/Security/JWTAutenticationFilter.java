package com.testPractico.Telconet.Security;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.testPractico.Telconet.Model.Login;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JWTAutenticationFilter extends UsernamePasswordAuthenticationFilter {

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
	Login credencials = new Login();
		try {
			credencials = new ObjectMapper().readValue(request.getReader(), Login.class);
		} catch (Exception e) {
			// TODO: handle exception
		}
		UsernamePasswordAuthenticationToken usernamePT = new UsernamePasswordAuthenticationToken(credencials.getEmail(),
				credencials.getContrasena(), Collections.emptyList());

		return getAuthenticationManager().authenticate(usernamePT);
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authresult) throws IOException, ServletException {

		UserDetailImpl detailImpl = (UserDetailImpl) authresult.getPrincipal();
		String token = TokenUtils.creaToken(detailImpl.getNombre(), detailImpl.getUsername());
		response.setContentType("application/json");
		PrintWriter writer = response.getWriter();
		String jsonResponse = "{\"token\": \"" + token + "\"}";
		writer.println(jsonResponse);
		response.addHeader("Authorization", "Bearer " + token);
		writer.close();
		response.getWriter().flush();
		super.successfulAuthentication(request, response, chain, authresult);
	}
}
