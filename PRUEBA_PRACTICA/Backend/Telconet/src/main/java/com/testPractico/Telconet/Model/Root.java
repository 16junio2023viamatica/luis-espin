package com.testPractico.Telconet.Model;

import java.util.List;

import lombok.Data;
import com.testPractico.Telconet.Entity.User;

@Data
public class Root
{
    public List<User> users;
   
}
