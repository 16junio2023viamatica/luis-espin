package com.testPractico.Telconet.Security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import lombok.AllArgsConstructor;

@Configuration
@AllArgsConstructor
public class WebSecurityConfig{
	
	private final UserDetailsService userDetailService;
	
	private final JWTAuthorizationFilter jwtAuthorizationFilter;
	
	
	@Bean
	SecurityFilterChain filterChain(HttpSecurity http,AuthenticationManager manager ) throws Exception {	
		
		JWTAutenticationFilter jwtAutenticationFilter = new JWTAutenticationFilter();
		jwtAutenticationFilter.setAuthenticationManager(manager);
		jwtAutenticationFilter.setFilterProcessesUrl("/login");
		return http.cors().and()
				.csrf().disable()
				.authorizeHttpRequests()
				.anyRequest()
				.authenticated()
				.and()
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
				.addFilter(jwtAutenticationFilter)
				.addFilterBefore(jwtAuthorizationFilter, JWTAutenticationFilter.class)
				.build();			
	}
	
	@Bean
	AuthenticationManager authManager(HttpSecurity http) throws Exception{
		return http.getSharedObject(AuthenticationManagerBuilder.class)
				.userDetailsService(userDetailService)
				.passwordEncoder(passwordEncoder())
				.and()
				.build(); 
	}
	
	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	

}
