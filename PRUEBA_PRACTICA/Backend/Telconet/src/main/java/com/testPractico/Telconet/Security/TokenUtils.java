package com.testPractico.Telconet.Security;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

public class TokenUtils {

	private final static String ACCESS_TOKEN_SECRET="efada9ced669563e233874040a5634fac80d41c51825697b8b1138191e11f801";
	private final static Long ACCESS_TOKEN_VALIDATE_TIME= 2_592_000L;

	public static String creaToken(String nombre, String Email)
	{
		long tokenTimeExpire = ACCESS_TOKEN_VALIDATE_TIME * 1_00;
		Date dateExpirate = new Date(System.currentTimeMillis()+tokenTimeExpire);
		 Map<String, Object> extra = new HashMap<>();
		 extra.put("nombre", nombre);
		 Claims claims = Jwts.claims().setSubject(Email);
		 return Jwts.builder()
				 .setSubject(Email)
				 .setExpiration(dateExpirate)
				 .setClaims(claims)
				 .signWith(Keys.hmacShaKeyFor(ACCESS_TOKEN_SECRET.getBytes()))
				 .compact();
	}
	
	public static UsernamePasswordAuthenticationToken getAuthentication(String token) 
	{
		try {
		Claims claims = Jwts.parserBuilder()
				.setSigningKey(ACCESS_TOKEN_SECRET.getBytes())
				.build()
				.parseClaimsJws(token)
				.getBody();
		String email = claims.getSubject();
		return new  UsernamePasswordAuthenticationToken(email,null,Collections.emptyList());
		}
		catch(JwtException e) {
			return null;
		}
	}
}
