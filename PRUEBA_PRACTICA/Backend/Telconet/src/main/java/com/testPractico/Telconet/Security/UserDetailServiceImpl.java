package com.testPractico.Telconet.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.testPractico.Telconet.Entity.User;
import com.testPractico.Telconet.Repository.UserRespository;



@Service
public class UserDetailServiceImpl  implements UserDetailsService{


	@Autowired
	UserRespository adminUsuarioRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User usuario = adminUsuarioRepository.finUsersForEmail(email);
				
		return new UserDetailImpl(usuario);
	}



}
