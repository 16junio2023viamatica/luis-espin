package com.testPractico.Telconet.Services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.testPractico.Telconet.Entity.User;
import com.testPractico.Telconet.Repository.UserRespository;

@Service
public class UserService {

	@Autowired
	private UserRespository respository;

	public User saveUser(User user) {
		return respository.save(user);
	}

	public List<User> saveUsers(List<User> users) {
		return respository.saveAll(users);
	}

	public List<User> ListaUsuario() {
		return respository.findActiveUsers();
	}

	@Transactional
	public void deactivateUser(Long userId) {
		respository.deactivateUser(userId);
	}
	public boolean findEmail(String email) {
		User user=respository.finUsersForEmail(email);
		if (user!=null) 
			return true;
		return false;
	}
	

}
