package com.testPractico.Telconet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.testPractico.Telconet.Entity.User;
import com.testPractico.Telconet.Services.UserService;

@Component
public class postInit implements CommandLineRunner {
	private final UserService service;
	
	public postInit(UserService userService) {
        this.service = userService;
    
    }

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
        System.out.println("¡La aplicación se está ejecutando desde la línea de comandos!");

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        User usuariodefault = new User();
        usuariodefault.setFirstName("Luis");
        usuariodefault.setLastName("Espin");
        usuariodefault.setEmail("luis@gmail.com");
        String clave =encoder.encode("12345");
        usuariodefault.setContraseña(clave);
        boolean existe = service.findEmail("luis@gmail.com");
        if (!existe) 
        service.saveUser(usuariodefault);
        
        
        
        
	}

}
