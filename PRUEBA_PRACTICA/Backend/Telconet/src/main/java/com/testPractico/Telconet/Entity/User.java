package com.testPractico.Telconet.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "users",schema = "public")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
    
    @Column(name = "firstname")
    public String firstName;
    
    @Column(name = "lastname")
    public String lastName;
    
    @Column(name = "hijos")
    public int hijos=0;
    
    @Column(name = "age")
    public Integer age;
    
    @Column(name = "email")
    public String email;
    
    @Column(name = "contraseña")
    public String contraseña;
    
    public String image;
    
    @Column(columnDefinition = "boolean DEFAULT '1'")
    public boolean status=true;
    
    @Column(name = "telefono")
    private String telefono;

    @Column(name = "nacimiento")
    private Date nacimiento;

    @Column(name = "civil")
    private String civil;

    @Column(name = "observacion")
    private String observacion;

}
