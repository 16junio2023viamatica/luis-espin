package com.testPractico.Telconet.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.testPractico.Telconet.Model.Root;
import com.testPractico.Telconet.Services.UserService;

@RestController
@RequestMapping("/api")
public class GetDataFront {

	@Autowired
	private UserService service;
	
	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public String SaveBDD(@RequestBody Root data) {
		service.saveUsers(data.getUsers());
		return "Exito"	;
	}
}
