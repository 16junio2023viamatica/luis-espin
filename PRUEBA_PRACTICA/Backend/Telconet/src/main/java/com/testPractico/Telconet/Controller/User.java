package com.testPractico.Telconet.Controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.testPractico.Telconet.Services.UserService;


@RestController
@RequestMapping("/user")
public class User {

	@Autowired
	private UserService service;
	
	@GetMapping("lista")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> getAllUserBDD() {
		List<com.testPractico.Telconet.Entity.User> lista =service.ListaUsuario();
		 return new ResponseEntity<>(lista, HttpStatus.OK);
	}

	@GetMapping("eliminar/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> DesactivarUsuario(@PathVariable Long id) {
		 service.deactivateUser(id);
		 return new  ResponseEntity<>("desactivado", HttpStatus.OK);
	}
	@PostMapping("actualiza")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> actualizaUsuario(@RequestBody com.testPractico.Telconet.Entity.User data) {
		com.testPractico.Telconet.Entity.User user=service.saveUser(data);
		 return new  ResponseEntity<>(user, HttpStatus.OK);
	}
}
