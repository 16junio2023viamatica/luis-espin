package com.testPractico.Telconet.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.testPractico.Telconet.Entity.User;

public interface UserRespository extends JpaRepository<User, Long> {

	@Modifying
	@Query("UPDATE User u SET u.status = false WHERE u.id = :userId")
	void deactivateUser(@Param("userId") Long userId);

	@Query("SELECT u FROM User u WHERE u.status = true")
	List<User> findActiveUsers();
	@Query("SELECT u FROM User u WHERE u.email = :email")
	User finUsersForEmail(@Param("email")String email);
}
