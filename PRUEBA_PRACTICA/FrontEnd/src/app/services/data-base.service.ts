import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataBaseService {

  urlBackend='http://localhost:8080';

  constructor(private http:HttpClient) {

  }
  llenarDatabase(body:any):Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
  return this.http.post<any>(this.urlBackend+'/api',body,{ headers });
  }
  listaUser():Observable<any>{
    return this.http.get<any>(this.urlBackend+'/user/lista');
  }
  usuarioDesactivado(id:any):Observable<any>{
    debugger
    return this.http.get<any>(this.urlBackend+'/user/eliminar/'+id);
  }

  actualizaUsuario(body:any):Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
  return this.http.post<any>(this.urlBackend+'/user/actualiza',body,{ headers });
  }

}
