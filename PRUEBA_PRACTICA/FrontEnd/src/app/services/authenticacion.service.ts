import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticacionService {

  urlBackend='http://localhost:8080';
  constructor(private http: HttpClient) { }
  login(body: any): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post<any>(this.urlBackend + '/login', body, { headers });
  }
}
