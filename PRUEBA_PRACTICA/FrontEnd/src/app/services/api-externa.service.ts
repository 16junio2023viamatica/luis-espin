import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiExternaService {

  urlApiServices:string="https://dummyjson.com/users";

  constructor(private http:HttpClient) {

  }

  traerData():Observable<any>{
    return this.http.get<any>(this.urlApiServices);
  }
}
