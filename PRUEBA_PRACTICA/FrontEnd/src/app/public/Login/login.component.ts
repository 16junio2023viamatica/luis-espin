import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, throwError } from 'rxjs';
import { AuthenticacionService } from 'src/app/services/authenticacion.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hide = true;
  login = {
    email: '',
    contrasena: ''
  };

  constructor(private router: Router, private logear: AuthenticacionService) { }

  ngOnInit(): void {
  }

  Logear() {
    if (this.login.contrasena == "" || this.login.email == "") {
      console.log(this.login);
      return;
    }
    this.logear.login(this.login).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 403) {
          console.log('Acceso denegado');
          return throwError('403');
        }
        return throwError('Algo salió mal');
      })
    )
      .subscribe({
        next: (response) => {
          localStorage.setItem("token", response.token);
          this.router.navigate(['/users']);
        },
        error: (error) => {
          if (error === '403') {
            console.log(error)
          }

        }
      }
      );
  }
  registrar() {

  }

}
