import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './Login/login.component';
import { NotFoundComponent } from '../core/components/not-found/not-found.component';
import { UserComponent } from './User/user.component';
import { GuarAuthGuard } from '../services/guardians/GuarAuthGuard';

const routes: Routes = [
  {path:'',redirectTo:'login',pathMatch: 'full'},
  {path:'login',component: LoginComponent},
  {path:'users',component: UserComponent,canActivate:[GuarAuthGuard],canLoad:[GuarAuthGuard]},
  {path:'**',component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
