import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { concatMap, switchMap } from 'rxjs/operators';
import { ApiExternaService } from 'src/app/services/api-externa.service';
import { DataBaseService } from 'src/app/services/data-base.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  data: any;
  dataSource: any;
  isDialogOpen: boolean = false;
  cargar = false;
  adicional = false;
  estadoCivil = [
    { value: 'soltero', viewValue: 'Soltero', active: false },
    {
      value: 'casado', viewValue: 'Casado', active: true
    }
  ]
  usuarioSelect = {
    firstName: '',
    lastName: '',
    age: '',
    email: '',
    image: '',
    telefono: '',
    nacimiento: '',
    civil: '',
    hijos: 0,
    observacion: '',
    status: false
  };



  constructor(private service: ApiExternaService, private dataBase: DataBaseService, private router: Router) {
  }

  ngOnInit(): void {

    this.cargaTabla();
  }
  llenaDatabase() {
    this.cargar = true;
    this.service.traerData()
      .pipe(
        concatMap(response => {
          this.data = response;
          return this.dataBase.llenarDatabase(this.data);
        })
      )
      .subscribe({
        next: (response) => {
          this.data = response;
          console.log(response);
          this.cargaTabla();
        },
        error: (error) => {
          if (error === '403') {
            console.log(error)
          }

        }
      }
      );

  }


  openDialog(user: any) {
    this.adicional = true;
    this.usuarioSelect = user;
    console.log(user);

    this.isDialogOpen = true;
  }

  validarCorreo(email: string): boolean {
    const regex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    if (!regex.test(email)) {
      return true;
    }
    return false;

  }

  editarUsuario(user: any) {
    this.adicional = false;
    this.usuarioSelect = user;
    this.isDialogOpen = true;
    }
  closeDialog() {
    this.adicional = false;
    this.isDialogOpen = false;
  }

  logout():void{
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
  guardaAdiccional(user: any) {
    if (this.validarCorreo(this.usuarioSelect.email)&&!this.adicional) {
      alert("Correo Invalido");
      return;
    }
    this.dataBase.actualizaUsuario(user).subscribe({
      next: (m) => {

        this.closeDialog();
      },
      error: (error) => {
        if (error === '403') {
          console.log(error)
        }

      }
    });

  }

  deleteUser(user: any) {
    this.dataBase.usuarioDesactivado(user.id).subscribe({
      next: (m) => {
        console.log(m);
      },
      error: (error) => {
        if (error === '403') {
          console.log(error)
        }

      }
    });

  }
  cargaTabla() {
    this.dataBase.listaUser()
      .subscribe({
        next: (response) => {
          console.log(response);
          this.dataSource = response;
        },
        error: (error) => {
          if (error === '403') {
            console.log(error)
          }

        }
      });
  }
}
