package proyeto_Algoritmo2;

import java.util.Random;
import java.util.Scanner;

public class Run {

	public static void main(String[] args) {
		Random random = new Random();
		Scanner scanner = new Scanner(System.in);
		System.out.print("Ingrese un Cadena: ");
		String cadena = scanner.nextLine();
		
		 // Verificar la longitud de la cadena
        if (cadena.length() % 2 == 0) {
            // Si la longitud es par
            int longitud = cadena.length();
            int mitad = longitud / 2;

            // Obtener las dos partes de la cadena
            String primeraParte = cadena.substring(0, mitad);
            String segundaParte = cadena.substring(mitad);

            // Realizar la concatenación invirtiendo las partes
            String nuevaCadena = segundaParte + primeraParte;

            System.out.println("Cadena original: " + cadena);
            System.out.println("Nueva cadena (par): " + nuevaCadena);
        } else {
            // Si la longitud es impar
            int longitud = cadena.length();

            System.out.println("La cadena ingresada tiene una longitud impar.");
            System.out.println("Longitud de la cadena: " + longitud);
        }

	}

}
