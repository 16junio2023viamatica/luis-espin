/**
 * 
 */
package algoritmo1;

import java.util.Random;
import java.util.Scanner;

/**
 * @author Luis
 *
 */
public class Run {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Random random = new Random();
		char letra;
		Scanner scanner = new Scanner(System.in);
		System.out.print("Ingrese un Letra Provincia: ");
		String letraProvincia = scanner.nextLine();
		System.out.print("Es comercial ? (Y/N): ");
		String tipoVehiculo = scanner.nextLine();
		if (tipoVehiculo.toUpperCase().equals("Y")) {
			tipoVehiculo = "A";
		} else {
			do {
				letra = (char) (random.nextInt(26) + 'A');
			} while (letra == 'A');
			tipoVehiculo = String.valueOf(letra);
		}
		System.out.print("Ingrese numero bloques: ");
		int bloques = Integer.parseInt(scanner.nextLine());
		for (int i = 1; i <= bloques; i++) {
			for (int k = 0; k < 3; k++) {
				int numeroEntero = random.nextInt(7) + 3;
				// Generar nombre del grupo
				String nombreGrupo = letraProvincia.toUpperCase() + tipoVehiculo.toUpperCase() + numeroEntero;

				// Generar nombre del paquete
				String nombrePaquete = "-";
				for (int j = 4; j <= 7; j++) {
					int numeroAleatorio = (int) (Math.random() * 10);
					nombrePaquete += numeroAleatorio;
				}

				// Mostrar los nombres generados
				System.out.println("Bloque "+i+": " + nombreGrupo + nombrePaquete);
			}
		}

	}

	public static void gruopos() {}
}
